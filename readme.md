# Graph analysis-based logic locking
#### Authors: Brice Colombier, Lilian Bossuet, David H�ly
#### Laboratoire Hubert Curien
#### 18 rue Pr. Beno�t Lauras, 42000 Saint-Etienne, FRANCE

Source code associated to [this research article](https://hal.archives-ouvertes.fr/ujm-01180564)
