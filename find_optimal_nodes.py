# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: find_optimal_nodes.py
# Date: 2016-10-13


def find_optimal_nodes(self, prim_out):

    """Find the optimal nodes to lock"""

    interesting_nodes = []
    mean_dist_to_out = []
#    color_optimal = "#EE0000"
    color_optimal = "#DDDDDD"
    h = self.clusters(mode="WEAK")
    # For every cluster
    for i in h:
        # Save the names of the nodes in the cluster
        cluster = self.vs[i]["name"]
        # Find the outputs in the cluster
        outputs_list = [temp for temp in cluster if temp in prim_out]
        source_vertices = []
        dist_list = []
        # If there is more than one output in the cluster
        if len(outputs_list) != 1:
            # Find source vertices
            for j in cluster:
                if not self.incident(j, mode="IN"):
                    source_vertices.append(j)
            # If there is only one source vertex, it locks all the outputs
            if len(source_vertices) == 1:
                node = source_vertices.pop()
                self.vs.find(node)["color"] = color_optimal
                self.vs.find(node)["cat"] = "lock"
                interesting_nodes.append(node)
            # If there are multiple source vertices
            else:
                # Compute the distance from source vertices to outputs
                for j in source_vertices:
                    # dist_list : ['node name', [distances to outputs]]
                    dist_list.append([j, list(map(len, self.get_shortest_paths(j, outputs_list)))])
                one_source = False
                for j in reversed(dist_list):
                    # If one source vertex is connected to all the outputs
                    if 0 not in j[1]:
                        self.vs.find(j[0])["color"] = color_optimal
                        self.vs.find(j[0])["cat"] = "lock"
                        interesting_nodes.append(j[0])
                        one_source = True
                        break
                # If there is no source vertex spanning all the outputs
                if not one_source:
                    nb_locked_outputs = []
                    len_dist_list_1 = len(dist_list[0][1])
                    for j in dist_list:
                        nb_locked_outputs.append(len_dist_list_1 - j[1].count(0))
                    # Sort the cluster members according to
                    # the number of outputs they lock
                    dist_list = list(list(zip(*sorted(zip(nb_locked_outputs, dist_list))))[1])
                    detect_0s = dist_list[0][1]
                    for j in range(1, len(dist_list)):
                        detect_0s = [sum(x) for x
                                     in zip(detect_0s, dist_list[j][1])]
                    # Sum the distances to outputs
                    # A zero here means an output is not reachable
                    if detect_0s.count(0):
                        print "Some outputs can not be locked"
                        print "An error occured during analysis"
                    else:
                        # reuses detect_0s for another purpose
                        detect_0s = []
                        node = dist_list.pop()
                        self.vs.find(node[0])["color"] = color_optimal
                        self.vs.find(node[0])["cat"] = "lock"
                        interesting_nodes.append(node[0])
                        detect_0s = node[1]
                        # Store the previous number of zeroes
                        # i.e. unlocked outputs
                        nb_0s = detect_0s.count(0)
                        while detect_0s.count(0):
                            node = dist_list.pop()
                            nb_0s = detect_0s.count(0)
                            detect_0s = [sum(x) for x
                                         in zip(detect_0s, node[1])]
                            # If this node allows to lock more outputs:
                            # i.e. it removes zeroes from detect_0s
                            if detect_0s.count(0) < nb_0s:
                                self.vs.find(node[0])["color"] = color_optimal
                                self.vs.find(node[0])["cat"] = "lock"
                                interesting_nodes.append(node[0])
        # If there is only one output in the cluster, pick the farthest node
        else:
            for j in cluster:
                dist_list.append(self.eccentricity(j, mode="OUT"))
            # Sort the cluster members according to their distance to outputs
            cluster = list(list(zip(*sorted(zip(dist_list, cluster))))[1])
            node = [temp for temp in cluster if temp not in outputs_list].pop()
            interesting_nodes.append(node)
            self.vs.find(node)["color"] = color_optimal
            self.vs.find(node)["cat"] = "lock"

        # Compute the distance from selected nodes to outputs
        for i in interesting_nodes:
            if self.get_shortest_paths(i, outputs_list) != [[]]:
                mean_dist_to_out.extend([o for o
                                         in map(len, self.get_shortest_paths(i, outputs_list))
                                         if o != 0])
    
    # To store the types of gates to insert
    types = []
    for i in interesting_nodes:
        types.append(self.vs.find(i)["locks"])
    return list(zip(interesting_nodes, types))
