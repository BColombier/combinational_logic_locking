# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label_inputs.py
# Date: 2016-10-13


def label_inputs(self):

    """Labels the inputs correctly so that they are not deleted afterwards"""

    for i in self.vs:
        if i["cat"] == "input":
            if i["locks"]:
                i["forced"] = [i["locks"][0]]

    return self
