# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: clean.py
# Date: 2016-10-13


def clean(self):

    """Remove unconnected vertices"""

    to_delete = []

    for i in self.vs:
        if self.neighbors(i, mode="ALL") == []:
            to_delete.append(i["name"])

    self.delete_vertices(to_delete)
    return self
