# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label_lin.py
# Date: 2016-10-13


def label_lin(self):

    """Labelling the graph according to linear (XOR/XNOR) gates"""

    for i in self.vs:
        # If the vertex has incoming edges
        predec = i.predecessors()
        if self.incident(i, mode="IN"):
            # If the incoming edges are of xor or xnor type
            if self.es.find(self.incident(i, mode="IN")[0])["name"] == "xor":
                forced = [predec[0]["forced"], predec[1]["forced"]]
                # If inputs are forced to the same value
                if forced == [[0], [0]] or forced == [[1], [1]]:
                    i["forced"] = [0]
                    self.delete_edges(self.incident(i, mode="IN"))
                # If inputs are forced to a different value
                elif forced == [[0], [1]] or forced == [[1], [0]]:
                    i["forced"] = [1]
                else:
                    raise Exception("The netlist probably contains too many XOR/XNOR gates.")
            
            elif self.es.find(self.incident(i, mode="IN")[0])["name"] == "xnor":
                forced = [predec[0]["forced"], predec[1]["forced"]]
                # If inputs are forced to the same value
                if forced == [[0], [0]] or forced == [[1], [1]]:
                    i["forced"] = [1]
                # If inputs are forced to a different value
                elif forced == [[0], [1]] or forced == [[1], [0]]:
                    i["forced"] = [0]
                else:
                    raise Exception("The netlist probably contains too many XOR/XNOR gates.")

    return self
