# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: modify_nodes_lock.py
# Date: 2016-10-13


def modify_nodes_lock(self, list_nodes_to_lock):

    """Modify the graph nodes to make them lockable"""

    lock = {0: "and",
            1: "or"}

    # Handle vertices
    for i in list_nodes_to_lock:
        attributes = self.vs.find(i[0]).attributes()
        attributes["name"] = "K"+i[0]
        attributes["label"] = attributes["name"]
        self.add_vertex(name=attributes["name"])
        for j in attributes:
            self.vs[len(self.vs)-1][j] = attributes[j]
        self.vs[len(self.vs)-1]["color"] = "orange"
        self.vs[len(self.vs)-1]["cat"] = "input"
        attributes["name"] = i[0]+"_mod"
        attributes["label"] = attributes["name"]
        self.add_vertex(name=attributes["name"])
        for j in attributes:
            self.vs[len(self.vs)-1][j] = attributes[j]
        self.vs[len(self.vs)-1]["color"] = "lightblue"
        self.vs[len(self.vs)-1]["cat"] = "mod"
    # Handle edges
    for i in list_nodes_to_lock:
        # Copy outgoing edges to mod node
        for j in self.incident(self.vs.find(i[0]), mode="OUT"):
            self.add_edge(i[0]+"_mod", self.vs[self.es[j].target]["label"],
                          label=self.es[j]["label"],
                          width=5,
                          arrow_size=2,
                          label_size=30,
                          color="#FF0000",
                          cat="lock")
    edges_to_delete = []
    for i in list_nodes_to_lock:
        # Delete copied edges from the original node
        for j in self.incident(self.vs.find(i[0]), mode="OUT"):
            edges_to_delete.append((i[0], self.vs[self.es[j].target]["label"]))
    self.delete_edges(edges_to_delete)
    for i in list_nodes_to_lock:
        if i[1] in [[0], [1]]:
            # The node should be forced to 0
            # An AND gate will be used
            self.add_edge("K"+i[0], i[0]+"_mod",
                          label=lock[i[1][0]],
                          width=5,
                          arrow_size=2,
                          label_size=30,
                          color="#FF0000",
                          cat="lock")
            self.add_edge(i[0], i[0]+"_mod",
                          label=lock[i[1][0]],
                          width=5,
                          arrow_size=2,
                          label_size=30,
                          color="#FF0000",
                          cat="lock")
        else:
            raise ValueError("The node has no Vforced value !")
    
    return self
