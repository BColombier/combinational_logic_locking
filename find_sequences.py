# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: find_sequences.py
# Date: 2016-10-13


def find_sequences(self, prim_out):

    """Finds the nodes that can not propagate the locking value"""

    to_delete = []

    # Consider all edges
    # Delete the edge if the target vertex (i.e. node)
    # can not propagate a locking value
    for i in self.es:
        if not self.vs.find(i.target)["forced"][0] in self.vs.find(i.target)["locks"]:
            to_delete.append(i)
    self.delete_edges(to_delete)

    to_delete = []
    
    to_outputs = len(prim_out)*[[]]
    for i in self.vs:
        if not i["name"] in prim_out:
            # If the array of the distances to outputs is [[],[],...,[],[]]
            if self.get_shortest_paths(i, to=prim_out) == to_outputs:
                to_delete.append(i)
    
    self.delete_vertices(to_delete)
    return self
