# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: label.py
# Date: 2016-10-13

import label_nonlin
import label_lin
import label_notbuf
import label_inputs


def label(self):

    """Label the graph"""

    self = label_nonlin.label_nonlin(self)
    for _ in range(3):
        self = label_inputs.label_inputs(self)
        self = label_notbuf.label_notbuf(self)
    self = label_inputs.label_inputs(self)
    self = label_lin.label_lin(self)
    self = label_inputs.label_inputs(self)
    
    return self
