# Author: Brice Colombier
#         Laboratoire Hubert Curien
#         42000 Saint-Etienne - France
# Contact: b.colombier@univ-st-etienne.fr
# Project: Demonstrator
# File: erase_labels.py
# Date: 2016-10-13


def erase_labels(self, prim_out):
    
    """Erase labels on nodes"""

    for i in self.vs:
        if not i["name"] in prim_out:
            if not self.incident(i, mode="IN") == []:
                i["forced"] = []
            i["locks"] = []

    return self
